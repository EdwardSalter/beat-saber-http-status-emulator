export interface BeatmapInfo {
    _version:               string;
    _songName:              string;
    _songSubName:           string;
    _songAuthorName:        string;
    _levelAuthorName:       string;
    _beatsPerMinute:        number;
    _shuffle:               number;
    _shufflePeriod:         number;
    _previewStartTime:      number;
    _previewDuration:       number;
    _songFilename:          string;
    _coverImageFilename:    string;
    _environmentName:       string;
    _songTimeOffset:        number;
    _customData:            BeatmapInfoCustomData;
    _difficultyBeatmapSets: DifficultyBeatmapSet[];
}

export interface BeatmapInfoCustomData {
    _customEnvironment:     string;
    _customEnvironmentHash: string;
    _contributors:          any[];
}

export interface DifficultyBeatmapSet {
    _beatmapCharacteristicName: string;
    _difficultyBeatmaps:        DifficultyBeatmap[];
}

export interface DifficultyBeatmap {
    _difficulty:              string;
    _difficultyRank:          number;
    _beatmapFilename:         string;
    _noteJumpMovementSpeed:   number;
    _noteJumpStartBeatOffset: number;
    _customData:              DifficultyBeatmapCustomData;
}

export interface DifficultyBeatmapCustomData {
    _difficultyLabel: string;
    _editorOffset:    number;
    _editorOldOffset: number;
    _warnings:        any[];
    _information:     any[];
    _suggestions:     any[];
    _requirements:    any[];
}
