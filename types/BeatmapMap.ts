export interface BeatmapMap {
  _time: number;
  _BPMChanges: any[];
  _events: Event[];
  _notes: Note[];
  _obstacles: Obstacle[];
  _bookmarks: Bookmark[];
}

export interface Bookmark {
  _time: number;
  _name: string;
}

export interface Event {
  _time: number;
  _type: number;
  _value: number;
}

export interface Note {
  _time: number;
  _lineIndex: number;
  _lineLayer: number;
  _type: number;
  _cutDirection: number;
}

export interface Obstacle {
  _time: number;
  _lineIndex: number;
  _type: number;
  _duration: number;
  _width: number;
}
