export type Performance = {
  rawScore: number; // Current score without the modifier multiplier
  score: number; // Current score with modifier multiplier
  currentMaxScore: number; // Maximum score with modifier multiplier achievable at current passed notes
  rank: "SSS" | "SS" | "S" | "A" | "B" | "C" | "D" | "E"; // Current rank
  passedNotes: number; // Amount of hit or missed cubes
  hitNotes: number; // Amount of hit cubes
  missedNotes: number; // Amount of missed cubes
  passedBombs: number; // Amount of hit or missed bombs
  hitBombs: number; // Amount of hit bombs
  combo: number; // Current combo
  maxCombo: number; // Max obtained combo
  multiplier: number; // Current combo multiplier {1, 2, 4, 8}
  multiplierProgress: number; // Current combo multiplier progress [0..1)
  batteryEnergy: null | number; // Current amount of battery lives left. null if Battery Energy and Insta Fail are disabled.
  softFailed: Boolean; // Set to `true` when the player's energy reaches 0, but they can continue playing. See the `softFailed` event.
};

export interface GameStatusEvent {
  event: "hello" | "songStart";
  status: GameStatus;
}

export interface GameStatus {
  game: {
    pluginVersion: string; // Currently running version of the plugin
    gameVersion: string; // Version of the game the current plugin version is targetting
    scene: "Menu" | "Song" | "Spectator"; // Indicates player's current activity
    mode:
      | null
      | "SoloStandard"
      | "SoloOneSaber"
      | "SoloNoArrows"
      | "PartyStandard"
      | "PartyOneSaber"
      | "PartyNoArrows"
      | "MultiplayerStandard"
      | "MultiplayerOneSaber"
      | "MultiplayerNoArrows"; // Composed of game mode and map characteristic
  };
  beatmap: null | {
    songName: string; // Song name
    songSubName: string; // Song sub name
    songAuthorName: string; // Song author name
    levelAuthorName: string; // Beatmap author name
    songCover: null | string; // Base64 encoded PNG image of the song cover
    songHash: string; // Unique beatmap identifier. Same for all difficulties. Is extracted from the levelId and will return null for OST and WIP songs.
    levelId: string; // Raw levelId for a song. Same for all difficulties.
    songBPM: number; // Song Beats Per Minute
    noteJumpSpeed: number; // Song note jump movement speed, how fast the notes move towards the player.
    songTimeOffset: number; // Time in millis of where in the song the beatmap starts. Adjusted for song speed multiplier.
    start: null | number; // UNIX timestamp in millis of when the map was started. Changes if the game is resumed. Might be altered by practice settings.
    paused: null | number; // If game is paused, UNIX timestamp in millis of when the map was paused. null otherwise.
    length: number; // Length of map in millis. Adjusted for song speed multiplier.
    difficulty: string; // Translated beatmap difficulty name. If SongCore is installed, this may contain a custom difficulty label defined by the beatmap.
    difficultyEnum: "Easy" | "Normal" | "Hard" | "Expert" | "ExpertPlus"; // Beatmap difficulty
    notesCount: number; // Map cube count
    bombsCount: number; // Map bomb count. Set even with No Bombs modifier enabled.
    obstaclesCount: number; // Map obstacle count. Set even with No Obstacles modifier enabled.
    maxScore: number; // Max score obtainable on the map with modifier multiplier
    maxRank: "SSS" | "SS" | "S" | "A" | "B" | "C" | "D" | "E"; // Max rank obtainable using current modifiers
    environmentName: string; // Name of the environment this beatmap requested // TODO: list available names
  };
  performance: null | Performance;
  mod: {
    multiplier: number; // Current score multiplier for gameplay modifiers
    obstacles: false | "FullHeightOnly" | "All"; // No Walls (FullHeightOnly is not possible from UI, formerly "No Obstacles")
    instaFail: Boolean; // 1 Life (formerly "Insta Fail")
    noFail: Boolean; // No Fail
    batteryEnergy: Boolean; // 4 Lives (formerly "Battery Energy")
    batteryLives: null | number; // Amount of battery energy available. 4 with Battery Energy, 1 with Insta Fail, null with neither enabled.
    disappearingArrows: Boolean; // Disappearing Arrows
    noBombs: Boolean; // No Bombs
    songSpeed: "Normal" | "Slower" | "Faster" | "SuperFast"; // Song Speed (Slower = 85%, Faster = 120%, SuperFast = 150%)
    songSpeedMultiplier: number; // Song speed multiplier. Might be altered by practice settings.
    noArrows: Boolean; // No Arrows
    ghostNotes: Boolean; // Ghost Notes
    failOnSaberClash: Boolean; // Fail on Saber Clash (Hidden)
    strictAngles: Boolean; // Strict Angles (Requires more precise cut direction; changes max deviation from 60deg to 15deg)
    fastNotes: Boolean; // Does something (Hidden)
    smallNotes: Boolean; // Small Notes
    proMode: Boolean; // Pro Mode
    zenMode: Boolean; // Zen Mode
  };
  playerSettings: Partial<{
    staticLights: Boolean; // `true` if `environmentEffects` is not `AllEffects`. (formerly "Static lights", backwards compat)
    leftHanded: Boolean; // Left handed
    playerHeight: number; // Player's height
    sfxVolume: number; // Disable sound effects [0..1]
    reduceDebris: Boolean; // Reduce debris
    noHUD: Boolean; // No text and HUDs
    advancedHUD: Boolean; // Advanced HUD
    autoRestart: Boolean; // Auto Restart on Fail
    saberTrailIntensity: number; // Trail Intensity [0..1]
    environmentEffects: "AllEffects" | "StrobeFilter" | "NoEffects"; // Environment effects
    hideNoteSpawningEffect: Boolean; // Hide note spawning effect
  }>;
}
