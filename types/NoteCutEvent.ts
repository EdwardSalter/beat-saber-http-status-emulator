import { Performance } from "./GameStatus";

export const NoteCutDirectionMap: Record<number, NoteCutDirection> = {
  0: "Up",
  1: "Down",
  2: "Left",
  3: "Right",
  4: "UpLeft",
  5: "UpRight",
  6: "DownLeft",
  7: "DownRight",
  8: "Any",
};

export const NoteTypeMap: Record<number, NoteType> = {
  0: "NoteA",
  1: "NoteB",
  2: "Bomb",
};

type NoteCutDirection =
  | "Up"
  | "Down"
  | "Left"
  | "Right"
  | "UpLeft"
  | "UpRight"
  | "DownLeft"
  | "DownRight"
  | "Any"
  | "None"; // Direction the note is supposed to be cut in

export type NoteType = "NoteA" | "NoteB" | "Bomb";

export interface NoteCutEvent {
  event: "noteCut";
  performance: Performance;
  noteCut: {
    noteID: number; // ID of the note
    noteType: NoteType; // Type of note
    noteCutDirection: NoteCutDirection;
    noteLine: number; // The horizontal position of the note, from left to right [0..3]
    noteLayer: number; // The vertical position of the note, from bottom to top [0..2]
    speedOK: boolean; // Cut speed was fast enough
    directionOK: null | boolean; // Note was cut in the correct direction. null for bombs.
    saberTypeOK: null | boolean; // Note was cut with the correct saber. null for bombs.
    wasCutTooSoon: boolean; // Note was cut too early
    initialScore: null | number; // Score without multipliers for the cut. It contains the prehit swing score and the cutDistanceScore, but doesn't include the score for swinging after cut. [0..85] null for bombs.
    finalScore: null | number; // Score without multipliers for the entire cut, including score for swinging after cut. [0..115] Available in [`noteFullyCut` event](#notefullycut-event). null for bombs.
    cutDistanceScore: null | number; // Score for the hit itself. [0..15]
    multiplier: number; // Combo multiplier at the time of cut
    saberSpeed: number; // Speed of the saber when the note was cut
    saberDir: [
      // Direction the saber was moving in when the note was cut
      number, // X value
      number, // Y value
      number // Z value
    ];
    saberType: "SaberA" | "SaberB"; // Saber used to cut this note
    swingRating: number; // Game's swing rating. Uses the before cut rating in noteCut events and after cut rating for noteFullyCut events. -1 for bombs.
    timeDeviation: number; // Time offset in seconds from the perfect time to cut a note
    cutDirectionDeviation: number; // Offset from the perfect cut angle in degrees
    cutPoint: [
      // Position of the point on the cut plane closests to the note center
      number, // X value
      number, // Y value
      number // Z value
    ];
    cutNormal: [
      // Normal of the ideal plane to cut along
      number, // X value
      number, // Y value
      number // Z value
    ];
    cutDistanceToCenter: number; // Distance from the center of the note to the cut plane
    timeToNextBasicNote: number; // Time until next note in seconds
  };
}
