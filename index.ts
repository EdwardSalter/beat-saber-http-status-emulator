// poison https://eu.cdn.beatsaver.com/e570d226c15ad3f5c4b84cfffe3542e61facc502.zip
// https://github.com/opl-/beatsaber-http-status/blob/master/protocol.md
// note overlay: https://itsnovahere.github.io/hitoverlay/
// song info overlay: http://bs-overlay.bopl.cf/#/overlay?corner=topleft
// map format: https://bsmg.wiki/mapping/map-format.html#schemas

import express from "express";
import expressWs from "express-ws";
import * as path from "path";
import * as fs from "fs-extra";
import { BeatmapInfo } from "./types/BeatmapInfo";
import { GameStatusEvent } from "./types/GameStatus";
import EventEmitter from "events";
import { BeatmapMap, Note } from "./types/BeatmapMap";
import {
  NoteCutDirectionMap,
  NoteCutEvent,
  NoteTypeMap,
} from "./types/NoteCutEvent";

type GroupedNotes = [number, Note[]];

class MyEmitter extends EventEmitter {
  start() {
    this.emit("start");

    const info = MyEmitter.getBeatmapInfo(MAP);
    const map = MyEmitter.getMap(MAP);

    const notes = this.getNotes(map);

    const beatLength = (60 / info._beatsPerMinute) * 1000;

    this.loop(notes, beatLength, 0);
  }

  loop(notes: GroupedNotes[], beatLength: number, index: number) {
    if (index >= notes.length) {
      console.log("ALL NOTES DONE");
      return;
    }

    const nextNoteGroup = notes[index];
    const prevNoteAt = index === 0 ? 0 : notes[index - 1][0] * beatLength;
    const nextNoteIn = nextNoteGroup[0] * beatLength - prevNoteAt;

    setTimeout(() => {
      this.emit("note", nextNoteGroup[1]);
      this.loop(notes, beatLength, index + 1);
    }, nextNoteIn);
  }

  private getNotes(map: BeatmapMap): Array<GroupedNotes> {
    return map._notes.reduce<Array<GroupedNotes>>((notes, note) => {
      const existing = notes.find(([time]) => time === note._time);
      if (existing) {
        existing[1].push(note);
        return notes;
      }
      return [...notes, [note._time, [note]]];
    }, []);
  }

  private static getBeatmapInfo(map: string): BeatmapInfo {
    const filePath = path.join(__dirname, `maps/${map}/Info.dat`);
    return fs.readJsonSync(filePath) as BeatmapInfo;
  }

  private static getMap(map: string): BeatmapMap {
    const filePath = path.join(__dirname, `maps/${map}/ExpertStandard.dat`);
    return fs.readJsonSync(filePath) as BeatmapMap;
  }
}

const myEmitter = new MyEmitter();

const app = express();
const { app: wsApp } = expressWs(app);

const MAP = "Poison Nightcore";

const FAKE_STATUS_EVENT: GameStatusEvent = {
  event: "hello",
  status: {
    performance: null,
    playerSettings: {},
    game: {
      gameVersion: "1.17.0",
      mode: "SoloStandard",
      pluginVersion: "1.0.0",
      scene: "Song",
    },
    mod: {
      batteryEnergy: false,
      batteryLives: null,
      disappearingArrows: false,
      failOnSaberClash: false,
      fastNotes: false,
      ghostNotes: false,
      instaFail: false,
      multiplier: 1,
      noArrows: false,
      noBombs: false,
      noFail: false,
      proMode: false,
      obstacles: false,
      smallNotes: false,
      songSpeed: "Normal",
      songSpeedMultiplier: 1,
      strictAngles: false,
      zenMode: false,
    },
    beatmap: {
      bombsCount: 0,
      difficulty: "Expert",
      difficultyEnum: "Expert",
      environmentName: "somewhere",
      length: 120,
      levelAuthorName: "someone",

      levelId: "1",
      maxRank: "SS",
      maxScore: 10000,
      noteJumpSpeed: 1,
      notesCount: 1000,
      obstaclesCount: 0,
      paused: null,
      songAuthorName: "someone",
      songBPM: 169,
      songCover: null,
      songHash: "123",
      songName: MAP,
      songSubName: "something",
      start: null,
      songTimeOffset: 0,
    },
  },
};

app.get("/map/:id", (req, res) => {
  myEmitter.start();
  res.send("Hello");
});

wsApp.ws("/socket", (ws) => {
  myEmitter.on("start", () => {
    console.log("Sending start event");
    ws.send(
      JSON.stringify({
        ...FAKE_STATUS_EVENT,
        event: "songStart",
      })
    );
  });

  myEmitter.on("note", (notes: Array<Note>) => {
    notes.forEach((note) => {
      const event: NoteCutEvent = {
        event: "noteCut",
        performance: {
          batteryEnergy: 1,
          combo: 1,
          currentMaxScore: 1,
          hitBombs: 0,
          multiplier: 1,
          hitNotes: 1,
          rawScore: 1,
          maxCombo: 1,
          missedNotes: 0,
          multiplierProgress: 1,
          passedBombs: 0,
          passedNotes: 0,
          rank: "SS",
          score: 1,
          softFailed: false,
        },
        noteCut: {
          noteID: 1,
          multiplier: 1,
          noteCutDirection: NoteCutDirectionMap[note._cutDirection],

          cutDirectionDeviation: 0,
          cutDistanceScore: 1,
          cutNormal: [1, 1, 1],
          cutDistanceToCenter: 1,
          cutPoint: [1, 1, 1],
          directionOK: true,
          noteLayer: note._lineLayer,
          noteLine: note._lineIndex,
          noteType: NoteTypeMap[note._type],
          finalScore: null,
          initialScore: null,
          saberDir: [1, 1, 1],
          saberSpeed: 1,
          saberType: note._type === 0 ? "SaberA" : "SaberB",
          saberTypeOK: true,
          speedOK: true,
          swingRating: 1,
          timeDeviation: 1,
          timeToNextBasicNote: 1,
          wasCutTooSoon: false,
        },
      };
      console.log(`Sending note event`)
      ws.send(JSON.stringify(event));
    });
  });

  console.log("Sending hello event");
  ws.send(JSON.stringify(FAKE_STATUS_EVENT));
});

app.listen(6557, () => {
  console.log("API is active");
});
